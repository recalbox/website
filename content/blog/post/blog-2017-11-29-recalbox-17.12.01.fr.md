+++
date = "2017-12-01T16:00:00+02:00"
image = "/images/blog/2017-11-29-recalbox-17-12-01/recalbox-17.12.01-banner.png"
title = "Recalbox 17.12.01"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

+++

Salutations !

Aujourd'hui la nouvelle version **17.12.01** de Recalbox est disponible !

Voyons plus en détail les nouvelles fonctionnalités :

* Nous avons procédé à une grosse mise à jour des émulateurs. Donc Retroarch passe en version **1.6.9**, ScummVM en **1.10.0** "instable", DosBox en **0.74 r4063** (Merci à [lmerckx](https://gitlab.com/lmerckx)) et  **tous les cores libretro** ont été mis à jour dans leur dernière version ! Plus d'informations dans ce [blogpost dédié]({{<ref "blog-2017-11-17-bump-emulators.fr.md">}})   
_Pour votre information, le core **fba-libretro** est maintenant basé sur fba **v0.2.97.42**._   
_N'oubliez donc pas de mettre à jour votre romset avant de jouer. Les nouveaux fichiers **.dat** sont_ [disponibles ici](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro) !
* Dans les versions X86 & X86_64 de Recalbox, vous pouvez maintenant émuler la PSX avec les cores **mednafen_psx** ! Plus d'informations dans ce [blogpost dédié]({{<ref "blog-2017-11-11-x86-mednafen-psx.fr.md">}})
* L'émulateur Dolphin supporte dorénavant les raccourcis à base de Hotkeys. Vous pouvez également accéder au menu de configuration de ce dernier (souris obligatoire). Nous avons également amélioré la gestion des **wiimotes émulées** !
* Sur X86 & X86_64 nous avons ajouté **imlib2_grab**  pour vous permettre de prendre des screenshots en ligne de commande !
* Toujours sur X86 & X86_64, le menu de boot (GRUB) dispose désormais d'une option **"verbose"**, afin de vous/nous aider dans le support.
* Nous avons également corrigé un bug intervenant lors de la configuration de plusieurs réseaux wifi. Merci à [OyyoDams](https://gitlab.com/OyyoDams)
* Du coté des Odroid C2 & XU4, vous pouvez maintenant jouer à la N64 avec le plugin vidéo mupen64plus **GLideN64**  (défini par défaut sur XU4) !
* Et bien d'autres points que vous pourrez lire dans le [changelog](https://gitlab.com/recalbox/recalbox/blob/master/CHANGELOG.md) affiché dans votre Recalbox durant la mise à jour !


Plein de nouvelles fonctionnalitées sont en préparation.    
Alors restez connecté et profitez bien de cette nouvelle release ;)
