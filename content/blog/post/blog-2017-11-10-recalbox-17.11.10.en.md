+++
date = "2017-11-10T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.10/title-17.11.10.png"
title = "Recalbox 17.11.10"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"

+++

Dear fellow Recalboxers !

We keep the release rate quite high with this new **17.11.10** version of Recalbox !

Let's get in the depths of the new features :

* If you're following us, you've seen our tease on **Hyperion**, which turns your Recalbox into an ambilight-like system if you have the right hardware ! See more on this [dedicated blogpost]({{< ref "/blog/post/blog-2017-11-10-hyperion.en.md" >}})
* Add Petrock's controlblock quirk to make it recognize the 2 players. Thank you [@petrockblog](https://twitter.com/petrockblog)!
* You can now suffix a .scummvm to the directories name in the scummvm rom folder ! Once you do this, you won't have to browse in the subfolder to start the game. You can even scrap the directory for better visuals ! Remember you still need the .scummvm file inside the folder. For example : call your Monkey Island 2 folder `monkey2.scummvm`, scrape, and play ! Thanks [lmerckx](https://gitlab.com/lmerckx).
* New pads configured out of the box: Nintendo Wii U Pro Controller and Switch Pro Controller, 8bitdo FC30 Arcade (BT and USB), Thrustmaster T Mini Wireless and Orange Controller
* Added a new Lynx core (libretro-handy) by default. This new core should be less exacting with romsets !

On a side note, we've made a big internal change again on the Recalbox repository structure. This was a **major step** leading to a general upgrade of the Linux we're built on. The 2nd stage is almost ready and will be open for testers very soon, and the 3rd stage will be that major upgrade ! Oh did I forgot to mention that this 3rd step will bring Kodi 17 ?


We do keep our word with new releases, don't we ? ;)
