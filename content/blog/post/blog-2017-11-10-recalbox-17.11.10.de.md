+++
date = "2017-11-10T20:00:00+02:00"
image = "/images/blog/2017-11-01-recalbox-17.11.10/title-17.11.10.png"
title = "Recalbox 17.11.10"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "substring"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++

Liebe Recalboxer!

Mit der neuen Recalbox-Version **17.11.10** halten wir die Release-Frequenz hoch.

Schauen wir uns die neuen Features im Detail an:

* Wenn ihr uns folgt, habt ihr die Vorschau auf **Hyperion** schon gesehen. Damit könnt ihr eure Recalbox in ein Ambilight-ähnliches System verwandeln. Mehr Infos gibt es in [diesem Post]({{< ref path="blog-2017-11-10-hyperion.en.md" lang="en" >}}) (englisch).
* Petrocks Controlblock Quirk ermöglicht 2 Spieler. Danke, [@petrockblog](https://twitter.com/petrockblog)!
* Wähle .scummvm als Namensendung für Ordner im scummvm-Rom-Verzeichnis. Dann musst du nicht mehr in die Ordner navigieren, um das Spiel zu starten. Außerdem kannst du sie  scrapen, um schöne Visualisierungen zu erhalten. Beachte: eine .scummvm Datei muss dennoch im Ordner enthalten sein. Nenne deinen Monkey Island 2 Ordner beispielsweise `monkey2.scummvm`, scrape und spiele los! Danke, [lmerckx](https://gitlab.com/lmerckx)!
* Weitere Pads werden automatisch konfiguriert: Nintendo Wii U Pro Controller und Switch Pro Controller, 8bitdo FC30 Arcade (BT und USB), Thrustmaster T Mini Wireless und Orange Controller
* Neuer Lynx Core (standardmäßig libretro-handy). Dieser Core sollte weniger anspruchsvoll bei Rom-Sets sein.


Eine Randnotiz: Wir haben eine weitere, große, interne Veränderung der Repository-Struktur vorgenommen. Das war ein **großer Schritt** Richtung Upgrade des Linux-Systems, auf dem wir basieren. Die 2. Phase ist fast fertig und wird sehr bald für unsere Tester zur Verfügung stehen. Die 3. Phase wird das große Upgrade sein! Oh, habe ich erwähnt, dass die 3. Phase Kodi 17 beinhalten wird?


Wir halten unser Versprechen bezüglich der neuen Releases, oder? ;)