+++
date = "2018-03-16T01:00:00+02:00"
image = "/images/blog/2018-03-16-recalbox-18-03-16/recalbox-18.03.16-banner.jpg"
title = "Recalbox 18.03.16"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/Nachtgarm"
  gitlab = "https://gitlab.com/Nachtgarm"
  name = "Nachtgarm"
+++
Hallo liebe Retrogamer!

Der Recalbox-Frühling kommt etwas früher und mit sich bringt er Features, die ihr auf keinen Fall missen wollt!

Hier die Änderungen:

* ES: Neuer Karussell-Modus und neue Theming-Möglichkeiten! Schaut euch dazu das "recalbox-next Theme" mal an (UI Einstellungen > Theme > recalbox-next).

* ES: Kein weißer "Screen of Death" mehr, wenn der VRAM überläuft! Themer können endlich 1080p Hintergrundbilder verwenden.

* ES: Musik wird nun auch in Unterverzeichnissen gefunden. Weiterhin wurden einige Start-Fehler behoben. Kodi kann nur noch aus der System-Ansicht heraus gestartet werden. Eine WLAN-SSID Auswahl wurde hinzugefügt und der Fehler mit gelegentlich auftretendem schwarzem Bild beim Beenden von Emulatoren wurde auch behoben.

* segacd: Das ".chd" Datei-Format wird ab sofort unterstützt.

* Moonlight: Aktualisiert auf 2.4.6. Unterstützt wird nun GFE bis Version 3.12. GFE PC ist nun möglich und der Scraper wurde neu geschrieben. Ihr könnt nun also endlich GFE auf eurem PC aktualisieren! Scrapen resultiert nun in einem Mix aus Spieleinfos der TGDB und Cover-Bildern von GFE. Ihr könnt die Cover-Bilder also jetzt in GFE einstellen.

* recalbox.conf: Der "videomode" kann jetzt auf `auto` gesetzt werden. Das ist für Leute mit speziellen Bildschirmen wie Röhrenmonitore und sehr kleinen Displays gedacht. Die Recalbox erkennt, ob der Bildschirm 720p beherrscht. Falls ja, wird auf 720p skaliert. Ansonsten bleibt die Recalbox bei der Standardauflösung.

* ScummVM: MT-32 Emulation für bessere Musik in den PC Version von Spielen ist nun aktiv.

* DosBox: Aktualisiert auf r4076 und eine virtuelle Tastatur wurde hinzugefügt, sodass ihr eure Gamepad Buttons auf Tasten mappen könnt, um dann DOS Spiele mit dem Gamepad spielen zu können. Mehr Infos findet ihr [hier](https://gitlab.com/recalbox/recalbox/issues/363) (englisch). Oder für die Faulen: Drückt Strg+F1.

* FBA-LIBRETRO aktualisiert (einige Bugs behoben)

* Kleinerer Fehler bezüglich der Defaultnamen für MAME Roms behoben

* PPSSPP auf v1.5.4 aktualisiert

* Original Startanimation wiederhergestellt und weitere hinzugefügt, aus denen beim Start zufällig eine ausgewählt wird. Wir hoffen, sie gefallen euch ;)

* Kleiner Typo bei x68000 Multidisk mit .m3u-Dateien behoben

* Einige Spiele können nun vom Web-Manager aus gestartet werden

* MUSIC Einstellungsmöglichkeit für Netzlaufwerke in `/boot/recalbox-boot.conf` hinzugefügt


Wir wünschen euch viel Spaß mit der neuen Version und freuen uns über euer Feedback im Forum!
