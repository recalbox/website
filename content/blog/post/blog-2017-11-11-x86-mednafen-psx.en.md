+++
date = "2017-11-11T14:00:00+00:00"
image = "/images/blog/2017-11-11-mednafen-psx/recalbox-mednafen-psx-banner.png"
title = "New PSX cores on X86"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Hi pals,

The current only PSX core available on Recalbox do an amazing job on ARM boards.
But he can't manage enhanced rendering options on X86 builds.

So to fix this issue, we added the [libretro mednafen psx cores](https://github.com/libretro/beetle-psx-libretro).

The **mednafen_psx_hw** core is Enhanced OpenGL Version with OpenGL renderer.
It needs at least an OpenGL 3.1 compatible GPU.
If your hardware is not compatible, you can use the standard **mednafen_psx** core.

Here are some options available with these cores :

- OpenGL renderer
- Internal GPU Resolution (1x(native)/2x/4x/8x/16x/32x)
- Texture filtering (nearest/SABR/xBR/bilinear/3-point/JINC2)
- Internal color depth (dithered 16bpp (native)/32bpp)
- PGXP perspective correct texturing
- Widescreen mode hack
- CPU Overclock
- CHD games support

More informations on [libretro documentation](https://buildbot.libretro.com/docs/library/beetle_psx_hw/).

For your information, these cores needs to add some new bios. Here are the files to [add in your Recalbox](https://github.com/recalbox/recalbox-os/wiki/Add-system-bios-%28EN%29) :

- **8dd7d5296a650fac7319bce665a6a53c** - **scph5500.bin**
- **490f666e1afb15b7362b406ed1cea246** - **scph5501.bin**
- **32736f17079d0b2b7024407c39bd3050** - **scph5502.bin**

These cores are picky, so respect the file names.

We hope you'll enjoy these new cores and (re)discover the fantastic PSX games library with modern graphics.

## Internal GPU Resolution

[![Internal GPU Resolution](/images/blog/2017-11-11-mednafen-psx/resolution-thumb.png)](/images/blog/2017-11-11-mednafen-psx/resolution.png)

## Texture filtering

[![Textures filtering](/images/blog/2017-11-11-mednafen-psx/textures-thumb.png)](/images/blog/2017-11-11-mednafen-psx/textures.png)

## PGXP perspective correct texturing

[![PGXP perspective correct texturing](/images/blog/2017-11-11-mednafen-psx/perspective-thumb.png)](/images/blog/2017-11-11-mednafen-psx/perspective.png)
