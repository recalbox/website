+++
date = "2018-02-09T11:00:00+02:00"
image = "/images/blog/2018-02-09-recalbox-18-02-09/recalbox-18.02.09-banner.jpg"
title = "Recalbox 18.02.09"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"

+++

Wazaaaaa !

Ca fait un petit moment qu'on n'a pas fait de nouvelle version ... Le temps de profiter un peu des fêtes de fin d'année, se reposer un peu, et bosser sur les nouveautés !

Donc, quoi d'neuf docteur avec 18.02.09 ? Une liste des évolutions majeures :

* Ajout de pads supportés en natif dans Recalbox : ouya, micreal arcade dual et ipega9055
* Une nouvelle vidéo d'intro (merci @ian57)
* x86 (32 et 64 bits) profite de l'émulation de la DS grâce aux cores libretro desmume et melonds
* Le core Mame2010 est disponible pour toutes les plateformes sauf rpi0/1
* Vous pouvez monter votre NAS en WiFi dès le boot à présent. Configurez simplement le wifi et les partages en suivant le [wiki](https://github.com/recalbox/recalbox-os/wiki/Load-your-roms-from-a-network-shared-folder-%28EN%29#recalbox-version--41)
* Les power scripts ont été mis à jour (utilisés pour les bouton Power/Reset sur GPIO)
* Mise à jour de ScummVM et DOSBox
* Nouvel émulateur : ResidualVM [homepage](http://www.residualvm.org). Il ne peut exécuter que Grim Fandango, Escape from Monkey Island et Myst III. Pensez à bien lire la documentation du site de ResidualVM. Pour jouer à ces jeux, déposez les dans le répertoire scummvm avec l'extension `.residualvm`, comme vous l'auriez fait pour un jeu scummvm
* Ajout du Sharp x68000 - core libretro px68k. Je suis sûr que très peu de vous connaissaient ce système à l'époque ;)
* Ajout de la 3DO pour XU4 et x86 32/64

J'espère que vous apprécierez cette nouvelle version !

Et ... Oh mon Dieu ... C'est le moment de faire ce truc de Steve Job "One last thing ..."

Les Pi 2 et 3 ont gagné à la loterie et bénéficient de l'Amiga ! Yeah baby ! Merci à Vojega de notre communauté pour son superbe travail sur l'Amiga depuis des années, c'est enfin intégré dans Recalbox !
