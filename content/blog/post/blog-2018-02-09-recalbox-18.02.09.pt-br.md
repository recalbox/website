+++
date = "2018-02-09T11:00:00+02:00"
image = "/images/blog/2018-02-09-recalbox-18-02-09/recalbox-18.02.09-banner.jpg"
title = "Recalbox 18.02.09"

[author]
  github = "https://github.com/substring"
  gitlab = "https://gitlab.com/substring45"
  name = "Substring"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"

+++

Olá amigos !

Já faz um tempo deste o último lançamento ! Época de curtir os feriados, descansar e mesmo assim trabalhar em novas funcionalidades !

Então, o que esperar da versão 18.02.09 ? Eis a lista das maiores mudanças:

* Adicionado suporte a novos controles: ouya, micrel arcade dual e ipega9055
* Um novo vídeo de inicialização (obrigado @ian57)
* x86 (32 e 64 bits) ganharam emulação do DS com os cores libretro desmume e melonds
* Core Mame2010 está disponível para todas as placas exceto rpi0/1
* Agora você pode montar compartilhamentos NAS através da Wifi durante o boot. Basta configurar a wifi e compartilhamentos de acordo com a [wiki](https://github.com/recalbox/recalbox-os/wiki/Load-your-roms-from-a-network-shared-folder-%28EN%29#recalbox-version--41)
* Scripts de energia atualizados (para quando você for usar botões na GPIO para controles power/reset)
* Atualizados o Dosbox e o ScummVM
* Novo emulador: ResidualVM [homepage](http://www.residualvm.org). Ele pode rodar apenas Grim Fandango, Escape from Monkey Island e Myst III. Leia cuidadosamente a documentação no site do ResidualVM. Para jogar estes jogos, adicione-os ao diretório de roms **scummvm** com a extensão `.residualvm`, da mesma forma como é feito para um jogo scummvm
* Adicionado sharp x68000 - core libretro px68k. Aposto que poucos de vocês conhecem este sistema ;)
* Adicionado 3DO para XU4 e x86 32/64

Espero que vocês gostem desta nova versão !

E ... Oh meu Deus ... Hora da famosa frase "One last thing" do Steve Jobs ...

Pi2 e Pi3 são os sortudos que a receber suporte ao Amiga ! Yeah baby ! Graças ao nosso membro da comunidade Voljega e seu árduo trabalho no amiga para Recalbox por anos, agora está definitivamente integrado ao Recalbox !
