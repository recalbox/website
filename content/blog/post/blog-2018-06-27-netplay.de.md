+++
date = "2018-06-27"
image = "/images/blog/2018-06-27-netplay/recalbox-18.06.27-banner.jpg"
title = "Netplay"

[author]
  github = "https://github.com/Supernature2k"
  gitlab = "https://gitlab.com/Supernature2k"
  name = "Supernature2k"
[translator]
  github = "https://github.com/robsdedude"
  gitlab = "https://gitlab.com/robsdedude"
  name = "Robsdedude"
+++


Hier kommt ein neuer Herausforderer!!!

Habt ihr je davon geträumt, Retro-Spiele online zu spielen und Achievements/Trophäen freizuschalten? Ganz wie bei den Next-Gen-Konsolen?

Nun ja... Retroarch macht es möglich und Recalbox bringt es zu euch nach Hause; im altbekannten, nutzerfreundlichen Design!

Um loszulegen, geht ins Menü unter / Spieleinstellungen / Netplay Einstellungen.


[![netplay settings](/images/blog/2018-06-27-netplay/netplay-settings.png)](/images/blog/2018-06-27-netplay/netplay-settings.png)

Hier könnt ihr Netplay an- und ausschalten, euren Nutzernamen und den Port wählen.

Außerdem könnt ihr CRC32 Hashes zu eurer gamelist hinzufügen, um bessere Matchings in der Netplay-Lobby zu erzielen:

[![hash](/images/blog/2018-06-27-netplay/hash.png)](/images/blog/2018-06-27-netplay/hash.png)

Bitte seid euch bewusst, dass dies, gerade für größere ROM-Sets, eine ganze Weile dauern kann (es geht aber schneller als Scraping). Dennoch empfehlen wir Hashes, da sie die Kompatibilität mit euren Mitspielern deutlich erhöhen.  
Ihr könnt auch Hashes zu existierenden gamelists hinzufügen (dazu müsst ihr bereits gescraped haben).

Jetzt seid ihr startklar!!!

Startet die Spiele einfach mit dem X- anstelle des B-Buttons, um in den *Host*-Modus zu gelangen. Folgende Systeme unterstützen derzeit Netplay: fba_libretro, mame, mastersystem, megadrive, neogeo, nes, pcengine, sega32x, sg1000, supergrafx

*Bitte beachtet, dass es mit machen Cores zu Performance-Problemen kommen kann*

[![x](/images/blog/2018-06-27-netplay/x.png)](/images/blog/2018-06-27-netplay/x.png)


Gut, wir haben jetzt ein Spiel im Host-Modus gestartet. Aber wie tritt man einem bestehenden Spiel bei, fragt ihr euch vielleicht. Ganz einfach!

Drückt im Home-Screen (dort wo alle System angezeigt werden) einfach den X-Button. Euch wird eine neue Oberfläche angezeigt, die alle verfügbaren Netplay-Spiele anzeigt!

[![lobby](/images/blog/2018-06-27-netplay/lobby.png)](/images/blog/2018-06-27-netplay/lobby.png)

Für jedes Spiel seht ihr jetzt eine Menge Informationen:
  * Nutzername (mit kleinem Icon, ob das Spiel auf einer Recalbox gestartet wurde)
  * Land
  * Hash Übereinstimmung (ob ihr eine ROM mit identischem Hash auf eurer Recalbox habt)
  * Datei Übereinstimmung
  * Core
  * Verbinungs-Latenz und andere Informationen

Unten (und vor jedem Spielenamen in der Liste) wird euch eine *Prognose* angezeigt, wie wahrscheinlich eine Verbindung funktionieren würde:
  * Grün: ROM mit passendem Hash gefunden und Core ist ok. Höchstwahrscheinlich funktioniert es.
  * Blau: Hash stimmt nicht überein (manche ROMs haben gar keinen Hash, z.B. arcade ROMs) aber eine passende ROM wurde gefunden und der Core ist ok. Es *sollte* funktionieren.
  * Rot: Keine passende ROM gefunden, nicht unterstütztes System, kein passender Core: Funktioniert auf keinen Fall (das Spiel kann nicht gestartet werden).

Bereit, erster Spieler?
