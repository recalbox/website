+++
date = "2018-02-09T11:00:00+00:00"
image = "/images/blog/2018-02-09-amiga/recalbox-amiga-banner.png"
title = "Amiga"

[author]
  github = "https://github.com/voljega"
  gitlab = "https://gitlab.com/voljega"
  name = "Voljega"
+++

Bonjour à tous !

Après une longue campagne de tests, l'émulation de l'Amiga est enfin disponible dans Recalbox !

Voici les détails à propos des fonctionnalités:

* L'émulateur utilisé est [Amiberry version 2.1](https://github.com/midwan/amiberry).
* 2 systèmes sont gérés pour le moment : Amiga 500/600 (tous les deux dans le systeme a600) et l'Amiga 1200. L'amiga CD32 a besoin d'évolutions côté Amiberry (dans la future 2.5) et sera intégré plus tard.
* Les formats supportés sont les disquettes (.adf, chargement automatique multi-disquettes) et disque dur (WHDL)

Vous aurez besoin d'au moins un bios selon le système à émuler :

* Amiga 600 ADF  **82a21c1890cae844b3df741f2762d48d  kick13.rom**
* Amiga 600 WHDL **dc10d7bdd1b6f450773dfb558477c230  kick20.rom**
* Amiga 1200 (ADF et WHDL) **646773759326fbac3b2311fd8c8793ee  kick31.rom**

Pour plus de détails, pensez à consulter le readme.txt du répertoire de roms and la [page wiki](https://github.com/recalbox/recalbox-os/wiki/Amiga-on-Recalbox-%28EN%29).

Et nous ne pourrons jamais remercier assez Ironic et Wulfman pour leur investissement dans l'intégration et les tests !

Une petite vidéo pour vous rafraichir la mémoire de quelques uns des meilleurs jeux :

<iframe width="560" height="315" src="https://www.youtube.com/embed/SrO9iJp8e2g" frameborder="0" allowfullscreen></iframe>
