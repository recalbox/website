+++
date = "2017-03-05T16:39:32+02:00"
image = "/images/blog/2017-03-05-hidden/title-showhidden.jpg"
title = "Hide your games."
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
  
+++
<p dir="auto">Hi, today let's see an other improvement about a feature introduced in 4.0.0 release: the ability <strong>to hide files</strong> in EmulationStation.</p><p dir="auto">"Hidden files" is a cool feature that allows you hide specifics files, as bios etc... But for the moment, once the game hidden, you have no ways to display it in EmulationStation. Your have to manually edit your gamelist.xml file, to unhide the file.</p><p dir="auto">So, we decided to <strong>add an option to display these hidden files</strong>. Once this option activated, hidden games are display preceded by this icon: </p><img src="/images/blog/2017-03-05-hidden/eye_closed.jpg" alt="eye_closed"><p dir="auto">You can see here is a preview of this feature:</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-05-hidden/es_showhidden.jpg" target="_blank"><img src="/images/blog/2017-03-05-hidden/es_showhidden.jpg" alt="es_showhidden"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-05-hidden/es_showhidden_2.jpg" target="_blank"><img src="/images/blog/2017-03-05-hidden/es_showhidden_2.jpg" alt="es_showhidden_2"></a></div><p dir="auto">Here is a short demo video:</p><iframe width="560" height="315" src="https://www.youtube.com/embed/I9v-4OsDUZQ" frameborder="0" allowfullscreen=""></iframe>