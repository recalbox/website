+++
date = "2018-02-04T07:00:00+00:00"
image = "/images/blog/2018-02-04-residualvm/recalbox-residualvm-banner.png"
title = "ResidualVM"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
+++

Bonjour tout le monde,

Le moment est venu d'accueillir un nouvel émulateur au sein de Recalbox : **ResidualVM !**  

Il s'agit d'un projet frère de **ScummVM**, qui vous permet de jouer aux jeux utilisants le moteur **GrimE**. Trois jeux sont compatibles avec cet émulateur :  
**Grim Fandango**, **Escape from Monkey Island** et **Myst III - Exile**.  

Ce nouvel émulateur a été ajouté par [lmerckx](https://gitlab.com/lmerckx) comme un **second core ScummVM**. Le changement entre ScummVM et ResidualVM est automatique. En effet, la façon d'ajouter des jeux dans votre Recalbox est la même que pour ScummVM. La seule différence concerne l'extension de fichier à utiliser. Une fois dans le répertoire **/recalbox/roms/scummvm**, vous devrez nommer le fichier de raccourci **gameShortName.residualvm** à la place de **gameShortName.scummvm** (par exemple, pour Grim Fandango ce sera **grim.residualvm** ).  
Recalbox sélectionnera alors automatiquement le bon émulateur.  

Si vous voulez plus d'informations, merci de lire la [documentation de ResidualVM](http://www.residualvm.org/documentation/).  
Vous trouverez également **la liste de compatibilité** [ici](http://www.residualvm.org/compatibility/)


Nous espérons que vous allez apprécier ce nouvel émulateur et que vous allez (re)découvrir la fantastique ludothèque de **ResidualVM / GrimE**.

## Grim Fandango
[![grim01](/images/blog/2018-02-04-residualvm/grim01-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim01.jpg)
[![grim02](/images/blog/2018-02-04-residualvm/grim02-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim02.jpg)
[![grim03](/images/blog/2018-02-04-residualvm/grim03-thumb.jpg)](/images/blog/2018-02-04-residualvm/grim03.jpg)

## Escape from Monkey Island
[![monkey01](/images/blog/2018-02-04-residualvm/monkey01-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey01.jpg)
[![monkey02](/images/blog/2018-02-04-residualvm/monkey02-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey02.jpg)
[![monkey03](/images/blog/2018-02-04-residualvm/monkey03-thumb.jpg)](/images/blog/2018-02-04-residualvm/monkey03.jpg)

## Myst III - Exile
[![myst01](/images/blog/2018-02-04-residualvm/myst01-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst01.jpg)
[![myst02](/images/blog/2018-02-04-residualvm/myst02-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst02.jpg)
[![myst03](/images/blog/2018-02-04-residualvm/myst03-thumb.jpg)](/images/blog/2018-02-04-residualvm/myst03.jpg)
