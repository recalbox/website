+++
date = "2017-03-06T16:43:03+02:00"
image = "/images/blog/2017-03-06-manager2/Webmanager2.jpg"
title = "Recalbox Web Manager 2.0 is out!"
draft = false

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"

+++
<p dir="auto">Hi, today let us present you an awesome work done by a community member.</p><p dir="auto">I don't know if everyone know that, but the current recalbox release already integrates a <strong>web-interface</strong>, that allows you to manage, configure your recalbox.</p><p dir="auto">But, some months ago, <strong><a href="https://github.com/DjLeChuck" target="_blank">DjLeChuck</a></strong>, finding that the current manager was not complete enough decided to develop a new one.</p><p dir="auto">This new web-interface gives you more options, the abilities to <strong>manage your game's information</strong>, your <strong>screenshots</strong>, <strong>EmulationStation's activity</strong> and more !<br>DjLeChuck did an amazing work, and we are proud to introduce it to you with this 4.1.0 release.</p><p dir="auto">If you want help the development of this new tool, you can report issue on the <a href="https://github.com/DjLeChuck/recalbox-manager" target="_blank">github repository</a> or help on <a href="https://poeditor.com/projects/view?id=93183" target="_blank">the translation</a>.</p><p dir="auto"></p><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-06-manager2/es_manager_1.jpg" target="_blank"><img src="/images/blog/2017-03-06-manager2/es_manager_1.jpg" alt="es_manager_1"></a></div><div class="image-container"><a class="no-attachment-icon" href="https://www.recalbox.com/assets/images/blog/2017-03-06-manager2/es_manager_2.jpg" target="_blank"><img src="/images/blog/2017-03-06-manager2/es_manager_2.jpg" alt="es_manager_2"></a></div><p>You can still use the first version of the manager by settings in recalbox.conf :<br></p><p style="color:gray">## Recalbox Manager (http manager)<br>system.manager.enabled=1<br>## 1 or 2, depending on the manager version you wish<br>system.manager.version=1<br></p><p dir="auto">Here is a short demo video :</p><iframe width="560" height="315" src="https://www.youtube.com/embed/l1sxi1w1mVY" frameborder="0" allowfullscreen=""></iframe>