+++
date = "2017-12-01T16:00:00+02:00"
image = "/images/blog/2017-11-29-recalbox-17-12-01/recalbox-17.12.01-banner.png"
title = "Recalbox 17.12.01"

[author]
  github = "https://github.com/rockaddicted"
  gitlab = "https://gitlab.com/rockaddicted"
  name = "rockaddicted"
[translator]
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
  name = "nwildner"
  
+++

Olá pessoal!

Hoje a nova versão **17.12.01** do Recalbox está disponível !

Estes são os detalhes das novas funcionalidades :

* Fizemos uma grande atualização dos emuladores. O retroarch pulou para a versão **1.6.9**, ScummVM para a **1.10.0** "instável", DosBox para a **0.74 r4063** (Obrigado [lmerckx](https://gitlab.com/lmerckx)) e **todos os cores da libretro** para a última versão ! Mais detalhes nesta [postagem dedicada]({{<ref "blog-2017-11-17-bump-emulators.pt-br.md">}}) 
_Adicionalmente, o core **fba-libretro** é agora baseado no fba **v0.2.97.42**._
_Portanto, não esqueça de atualizar sua romset antes de jogar. Os novos arquivos **.dat** estão_ [disponíveis aqui](https://gitlab.com/recalbox/recalbox/tree/master/package/recalbox-romfs/recalbox-romfs-fba_libretro/roms/fba_libretro/clrmamepro) !
* No Recalbox X86 e X86_64, você pode emular o PSX utilizando os cores **mednafen_psx** ! Mais detalhes nesta [postagem dedicada]({{<ref "blog-2017-11-11-x86-mednafen-psx.pt-br.md">}})
* O emulador Dolphin agora suporta os atalhos de hotkey e você pode agora acessar a interface de configurações. Também melhoramos o suporte a **wiimotes emulados** !
* Nas versões X86 e X86_64 adicionamos um novo software, **imlib2_grab** para tirar screenshots pela linha de comando !
* Nas versões X86 e X86_64, novamente, o menu de boot (GRUB) possui uma opção **verbose** para ajudar vocês e nós no suporte.
* Também corrigimos um bug de configuração de diversas redes wifi. Obrigado [OyyoDams](https://gitlab.com/OyyoDams).
* Nas versões de Odroid C2 e XU4, você pode jogar N64 com o **plugin de vídeo GLideN64** do mupen64plus (habilitado por padrão no XU4) !
* Outros detalhes desta atualização podem ser lidos no [changelog](https://gitlab.com/recalbox/recalbox/blob/master/CHANGELOG.md) que também é exibido no seu Recalbox !


Diversas funcionalidades novas e emocionantes virão.
Então, fique ligado e divirta-se com esta nova versão ;)
