+++
date = "2019-04-12T07:00:46Z"
title = "Lançado o Recalbox 6.0"
image = "/images/blog/2019-04-12-recalbox-6.0-dragonblaze-released/banner.jpg"

[author]
  name = "Fab2Ris"
  facebook = "https://www.facebook.com/fabriceboutard"
  twitter = "https://twitter.com/Fab2Ris"

[translator]
  name = "Nícolas Wildner"
  github = "https://github.com/nwildner"
  gitlab = "https://gitlab.com/nwildner"
+++

Até que enfim, chegou 🙌

Vocês aguardaram durante meses, e nós trabalhamos por meses;

Estamos super orgulhosos de anunciar que está disponível a nova versão do Recalbox!

A última versão estável do Recalbox não é mais a `18.07.13`: é a…

## Recalbox 6.0: DragonBlaze

Como vocês já leram na nossa [postagem anterior sobre o Release Candidate]({{<ref "blog-2019-03-22-upcoming-stable-release.pt-br.md">}}), decidimos reverter para o esquema de numeração baseado em versão (utilizar versões baseadas em data causou muita confusão 😖), e a nova versão foi numerada `6.0`.

Também decidimos que algumas versões receberão apelidos. Estamos muito felizes de anunciar o lançamento da DragonBlaze 🐉 E a explicação da escolha do apelido não fará parte desta postagem… tente adivinhar 😉

## Créditos

Esta nova versão estável é o resultado de 9 meses de trabalho árduo de diversas pessoas, que são de vários países com os mais diversos perfis. Estamos orgulhosos desta diversidade e a encorajamos: todo mundo é bem-vindo e encorajado a ajudar 💪

O **time de desenvolvimento** se esforçou de forma substancial para embarcar novos emuladores, implementar funcionalidades e corrigir uma tremenda quantidade de bugs. Um grande salve para todos: membros fundadores, membros antigos e novatos.

Um salve também para todos os **contribuidores externos** que ajudaram de uma forma mais ou menos temporária, mas sempre agiram de maneira amistosa, ajudaram com sua expertise, tempo, ou boa vontade. Somos gratos por todas as contribuições, pequenas ou grandes, técnicas ou não, de código, design, documentação, … sem esquecer do nosso time de tradutores voluntários 🌐

Além do desenvolvimento, tentamos adotar ciclos mais dinâmicos de pré-lançamento, angariando o feedback de membros destemidos(early-adopters) que nos guiaram para a versão estável. As palavras chave aqui são "acumular feedback": nada disto seria possível sem a atenção infinda e paciência (quase) infinita do nosso **time de suporte**  nos canais de comunicação: [Discord](https://discord.gg/RR2jc5n) e [fórums](https://forum.recalbox.com) principalmente, mas também por [email](mailto:support@recalbox.com).

A terceira peça principal é o time de **designers** 🎨 O Recalbox não teria tanto sucesso se não fosse pela interface moderna, fluída e intuitiva. Um grande viva para nossos designers incansáveis ❤️

Finalmente, nosso trabalho não teria tanto valor se não fosse pela **comunidade** maravilhosa que temos: pessoas dando feedback, ajudando uns aos outros, desenvolvendo temas, contribuíndo com código, publicando artigos e vídeos, ou simplesmente elogiando. É um ecosistema vibrante e somos surpreendidos diariamente ao testemunhar o seu crescimento.

Obrigado a todos por fazer do Recalbox o que ele é hoje 🙏

## O que há de novo?

Vamos lá, ver o que realmente importa, certo?

O que há de novo? Porque devo atualizar para esta nova versão?

O novo changelog é muito longo e exaustivamente técnico para ser tratado aqui então, vamos ressaltar algumas alterações específicas que merecem o destaque 🎁

### Novas placas suportadas 

Provavelmente esta é a funcionalidade mas requisitada de todas! O Recalbox agora suporta oficialmente a placa **Raspberry Pi 3B+**. Esta é a nova placa da Raspberry Pi Foundation com alguns ganhos de desempenho e o mesmo preço acessível.

Mas esta não é a única nova placa suportada!

Na família da Raspberry, o suporte a placa menos conhecida **Compute Module 3** também foi adicionado.

Graças ao trabalho do @mrfixit2001, o Recalbox também está disponível para uma família inteira de placas mais potentes (leia: "alta performance") da [Pine64](https://www.pine64.org): **Rock64** e **Rock64Pro** (e placas compatíveis **RockBox** e **Rock Pi4**).

### Modo Demo

Uma funcionalidade antecipada especialmente para donos de bartops é o que chamamos de **modo demo**!

Ao ser escolhido como proteção de tela, o modo demo irá começar automaticamente assim que nenhum botão for pressionado por um determinado tempo (configurável).

Ele **selecionará e jogará um jogo aleatório da sua coleção** durante alguns minutos ✨

Sente e relaxe, pois iremos mostrar jogos que você nem lembrava que possuía!

E para uma experiência mais agradável, adicionamos um toque antes do lançamento: ao pressionar `Start` em qualquer dos controles o Recalbox passará o controle para você, que poderá continuar jogando de onde o demo parou 🕹

### Novos emuladores

O que seria do Recalbox sem os emuladores que possui?

Não desenvolvemos emuladores, mas trabalhamos duro para integrar e adaptar novos emuladores a todo momento para o Recalbox.

Mais de **30 novos emuladores** foram integrados, aumentando o número total de sistemas emulados para mais de 80 🚀 São eles: 

* Atari 5200
* Atari 8Bit
* Intellivision
* SamCoupé
* Fairchild Channel F
* Super Famicon SatellaView
* Amiga CD32
* NeoGeoCD
* Jaguar
* e muitos outros…

E os 50 emuladores que já faziam parte do grupo? Bom, **atualizamos todos eles** para suas versões mais recentes ✨

### Suporte a controles repaginado

Em mais uma implementação técnica, reescrevemos o código que gerencia a maioria dos controles, tomando cuidado especial com os controles bluetooth (e com um cuidado especial para os da família 8BitDo).

Adicionamos suporte  novos controles, incluindo o **GameCube MayFlash**, o **XinMo controller**, o **8BitDo M30** e muitos outros…

### Controle Adaptativo

Por último, mas não menos importante, esta funcionalidade pode não ser a mais impressionante tecnicamente falando, mas é a mais querida, pois acreditamos que video games e jogos retrô unem as pessoas.

Sempre guiamos o Recalbox com acessibilidade em mente: acessibilidade financeira (é uma solução livre e open-source para hardware barato), acessibilidade técnica (é fácil para iniciantes e uma solução plug-and-play) e acessibilidade histórica (pois é uma máquina do tempo para software legado e as vezes esquecido).

Alguns meses atrás, adicionamos a acessibilidade humana como nossa missão para tornar o Recalbox acessível a todos.

"Todo mundo" é um escopo muito amplo, mas tem algo que sabíamos que poderia ser feito para permitir que pessoas com dificuldades motoras pudessem jogar os mais de 40 mil jogos existentes nos 80 sistemas das últimas décadas.

Fizemos, e estamos orgulhosos disto.

No Recalbox 6.0 DragonBlaze adicionamos suporte oficial e plug-and-play para o recém lançado [**Xbox Adaptive Controller**](https://news.microsoft.com/stories/xbox-adaptive-controller) da Microsoft.

Acreditamos fortemente que este é um grande salto na integração de pessoas com dificuldades motoras e esperamos que isto possa unir mais as pessoas.

## Download

Gostaria de usar o Recalbox 6.0 DragonBlaze? 

[Baixe agora!](https://archive.recalbox.com) É livre, open-source, e vem com um suporte amistoso de um exército de voluntários apaixonados e sem objetivos comerciais (for fun!).

-----

