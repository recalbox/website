+++
date = "2017-02-18T15:56:54+02:00"
image = "/images/blog/2017-02-18-download/title.jpg"
title = "You broke our download server !! :D"
draft = false
[author]
  github = "https://github.com/digitallumberjack"
  gitlab = "https://gitlab.com/digitallumberjack"
  name = "digitalLumberjack"
  twitter = "https://twitter.com/digitalumberjak"

+++

<div><p>Hello guys,<br>so as you can see, our update servers are actually overloaded. You are too many users updating their recalbox, so servers have too many connextions in same time.<br>We were not prepared to this. We didn't have this kind of problem with previous releases.<br>So, we scaled the servers and added mirrors. Once cache filled on these mirrors, the sirtuation should come back to normal.<br>Thank you to be patient and for your understanding.<br>PS: For your information, once your recalbox on standby, the current download still progress. But once done, the screen won't refresh to display the reboot message.<br>So, sometimes, move your gamepad to wake up your recalbox ;)<br><br></p><p>You can follow the progression of the issue on twitter : <a href="https://twitter.com/recalbox">@recalbox</a></p></div>